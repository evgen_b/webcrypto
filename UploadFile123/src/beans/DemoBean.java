package beans;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException; // кто прочитал тот сдох
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.Part;

@ManagedBean
@SessionScoped

public class DemoBean 
{
    private Part file1;

    private  static String DIR = null;
    public Part getFile1()
    {
        return file1;
    }

    public void setFile1(Part file1)
    {
        this.file1 = file1;
    }


    private static void setDirUpload()
    {
    	ServletContext servletContext= (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        String path = servletContext.getRealPath("/");
        File fl = new File(path + "/upload");
        DIR = fl.getPath() + File.separator;
    }
    public String upload() throws IOException
    {
    	if (DIR == null)
    		setDirUpload();
        file1.write(DIR + getFilename(file1));
        crypto(file1);
        return "success?faces-redirect=true";
    }
    private static String getFilename(Part part) 
    {
        for (String file : part.getHeader("content-disposition").split(";")) {
            if (file.trim().startsWith("filename")) {
                String filename = file.substring(file.indexOf('=') + 1).trim().replace("\"", "");
                return filename.substring(filename.lastIndexOf('/') + 1).substring(filename.lastIndexOf('\\') + 1); // MSIE fix.
            }
        }
        return null;
    }
    static void fileProcessor(int cipherMode,String key,File inputFile,File outputFile){
        try {
            Key secretKey = new SecretKeySpec(key.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(cipherMode, secretKey);

            FileInputStream inputStream = new FileInputStream(inputFile);
            byte[] inputBytes = new byte[(int) inputFile.length()];
            inputStream.read(inputBytes);

            byte[] outputBytes = cipher.doFinal(inputBytes);

            FileOutputStream outputStream = new FileOutputStream(outputFile);
            outputStream.write(outputBytes);

            inputStream.close();
            outputStream.close();

        } catch (NoSuchPaddingException | NoSuchAlgorithmException
                | InvalidKeyException | BadPaddingException
                | IllegalBlockSizeException | IOException e) {
            e.printStackTrace();
        }
    }

    public static void crypto(Part file1) {
        String key = "This is a secret";
        ServletContext servletContext= (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        String path = servletContext.getRealPath("/");
        File inputFile = new File(path + "/upload/getFilename(file1)");
        File encryptedFile = new File(path + "/upload/text.encrypted");
        File decryptedFile = new File(path + "/upload/decrypted-text.txt");

        try {
            DemoBean.fileProcessor(Cipher.ENCRYPT_MODE,key,inputFile,encryptedFile);
            DemoBean.fileProcessor(Cipher.DECRYPT_MODE,key,encryptedFile,decryptedFile);
            System.out.println("Sucess");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
    }
}
